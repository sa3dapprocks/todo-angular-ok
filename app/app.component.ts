import { Component,OnInit} from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
//readonly ROOT_URL='http://jsonplaceholder.typicode.com';
readonly ROOT_URL='http://127.0.0.1:8000/ng';
posts:any;
constructor(private http: HttpClient){}
getPosts(){
  //this.posts = this.http.get(this.ROOT_URL+ '/posts');
  this.posts = this.http.get(this.ROOT_URL + '-todo')
}


addTodo(newTodoLabel){

  var newTodo= {
    title : newTodoLabel,
    status:false,
    priority:3

  };
  //this.todos.push(newTodo);
  this.http.post('http://127.0.0.1:8000/api/todo',newTodo).subscribe(data => {
      console.log(data);
    });

  console.log(newTodo);
}

deleteTodo(id){
  var x=id;
 this.http.get('http://127.0.0.1:8000/api/todo/delete/${id}').subscribe(data => {
      console.log(data);
    });
console.log(id);
}


  title = 'app';
  todos =[ {
  	label : 'bring milk',
  	done:false,
   	priority:3},
   	{
  	label : 'mobile pill',
  	done:true,
   	priority:3}
   	,{
  	label : 'clean house',
  	done:false,
   	priority:3},
   	{
  	label : 'fix bulb',
  	done:false,
   	priority:3},
   	 {
   	 	label : 'watch timmy ternar',
  	done:false,
   	priority:3
   	 }


];


addTodo2(newTodoLabel){
	var newTodo= {
		label : newTodoLabel,
  	done:false,
   	priority:3
	};
	this.todos.push(newTodo);
  console.log(newTodo);
}

deleteTodo2(todo){
	this.todos = this.todos.filter(t=>t.label !== todo.label);


}

}
